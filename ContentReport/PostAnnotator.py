from config import _SOURCE, CHANNELS_LIST

from pymongo import MongoClient
# client = MongoClient(_SOURCE['MONGO_HOST'], _SOURCE['MONGO_PORT'])

conn = MongoClient('mongodb://198.168.1.12:27017')
# client = MongoClient('mongodb://'+_SOURCE['MONGO_HOST']+':'+str(_SOURCE['MONGO_PORT'])+'/')

database = conn[_SOURCE['DATABASE_NAME']]
collection = database[_SOURCE['COLLECTION_NAME']]

# print collection.count()

# for x in collection.find():
#     print x 

# print collection.find_one()


ACCOUNT_IDs = ['307620662033']
# exit(0)

from esConnector import get, update
from InsightsEngine.TextGrip import TextGrip, TextGripUtility   

TG = TextGrip()
TGU = TextGripUtility()

for channel in CHANNELS_LIST:
    ### INPUT QUERY TO FETCH, PARSE AND UPDATE
    input_query = {
    	"size" : 1000,
        "query": {
            "bool": {
                "should": {
                    "bool": {
                        "must_not": [{
                            "exists" : {
                                "field" : "_ds"
                            }
                        }],
                        "must": [
                            {"terms": { "cType": [channel] }},
                            {"terms": { "aId": ACCOUNT_IDs }}
                        ]
                    }
                },
                "minimum_should_match": "1"
            }
        }
    }

    _config = {
    	'input_query' : input_query,
    	'size' : 100,
    	'scroll' : '2m',
    }


    documents = get._get_stream(_config)

    bulk_body = ''
    for document in documents:
        print document
        
        # if "m" in document:
        #     features = TG.annotate_post(document["m"])

        #     print features
        # else:
        #     features = {}

        

        # bulk_body += '{ "update" : {"_id" : "'+document['id']+'", "_index" : "' + document['_index'] + '", "_type" : "propheseePost"} }\n'
        # bulk_body += '{ "script": "ctx._source._ds = ' + str(features) +'"}\n'

        # if len(bulk_body.split("\n")) % 100 == 1:
        #     update._bulk_operation(bulk_body)
        #     bulk_body = ''