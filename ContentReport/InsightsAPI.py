# from InsightsEngine.Aggregator import Aggregator
from esConnector import get, update
import pandas as pd 

############### Content Break Down #######################

## Fetch Data 


## -- Run ES Queries and get Raw Responses 
## -- Combing -- DF 
## -- Write Conditions to get Insights 

ACCOUNT_IDs = ['307620662033', '1530735482','175865077', '2531735','1803483906','151123001']
input_query = {
    	"size" : 1000,
        "query": {
            "bool": {
                "should": {
                    "bool": {
                        "must": [
                            {"terms": { "cType": ['FACEBOOK'] }},
                            {"terms": { "aId": ACCOUNT_IDs }}
                        ]
                    }
                },
                "minimum_should_match": "1"
            }
        }
    }

_config = {
	'input_query' : input_query,
	'size' : 100,
	'scroll' : '2m',
}


documents = get._get_stream(_config)

htDF = pd.DataFrame()
for document in documents:
	breakdownDF['id'] = []
	if "hT" in document:
		print document["hT"]

## Aggregate 

## Generate Results 