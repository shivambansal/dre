from config import _SOURCE
import elasticsearch
source_es = elasticsearch.Elasticsearch([{'host':_SOURCE['ELASTIC_HOST'],'port':_SOURCE['ELASTIC_PORT']}])

class Get:
	def _get_stream(self, _config):
		c = source_es.count(index = _SOURCE['ELASTIC_INDEX'], body = _config['input_query'])
		res = source_es.search(index = _SOURCE['ELASTIC_INDEX'], body = _config['input_query'], scroll = _config['scroll'], size = _config['size'])

		i = 0
		scroll_size = len(res['hits']['hits'])
		print scroll_size
		while (scroll_size > 0):
			for post in res['hits']['hits']:
				i += 1 				
				pos = post['_source']	
				pos['_index'] = post['_index']		
				yield pos

			scroll_id = res['_scroll_id']
			res = source_es.scroll(scroll_id = scroll_id, scroll='2m')
			scroll_size = len(res['hits']['hits'])

class Update:
	def _update_document(self, dimension, _id, _index):
		try:
			source_es.update(index = _index, id = _id, doc_type = "propheseePost", body = {"doc": {"dimension": dimension}})
		except Exception as E:
			print "Update Failed due to - ", E 
			pass 

	def _bulk_operation(self, bulk_data):
		try:
			source_es.bulk(body = bulk_data)
		except Exception as E:
			print "Bulk Update Failed due to - ", E
			pass

get = Get()
update = Update()