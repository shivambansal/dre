import pandas as pd
import numpy as np
import collections
import random

class GenerateInsights:
	def __init__(self):
		self.df1 = pd.read_csv('InsightsEngine/dictionaries/insights/DescriptiveInsights.csv')
		self.df2 = pd.read_csv('InsightsEngine/dictionaries/insights/ToneInsights.csv')
		self.thresh = 0.25

	def get_message2(self, key1, key2):
		df = self.df2.copy()
		df = df[(df['EmotionalTone']==key1) & (df['Fills']==key2)]
		storylines = list(df['Storyline'])
		random.shuffle(storylines)
		return storylines[0]

	def get_message1(self,key,value):
		results = []
		df = self.df1.copy()
		df = df[df['feature']==key]
		df = df[(df['lower']<=value) & (df['upper']>=value)]
		if len(df)>0:
			df = df.reset_index()
			for x in df.values:
				results.append(x)
		return results
		
	def flatten(self, d, parent_key = '', sep ='_'):
		items = []
		for k, v in d.items():
			new_key = parent_key + sep + k if parent_key else k
			if isinstance(v, collections.MutableMapping):
				items.extend(self.flatten(v, new_key, sep=sep).items())
			else:
				items.append((new_key, v))
		return dict(items)

	def parse_insights(self, data):
		results = self.flatten(data, parent_key='', sep='.')
		
		for key, value in results.items():
			if type(value) == float or type(value) == int:
				key = key.split('.')[-1]

				if key in list(self.df1['feature']):      
					results = self.get_message1(key,value)
					
					if results:
						sec = results[0][3]

						insights = [_[7] for _ in results]
						suggestions = [_[8] for _ in results]
						
						random.shuffle(insights)
						random.shuffle(suggestions)

						ins = insights[0]
						sug = suggestions[0]

						if sec and sec in data:
							if ins != None and str(ins) != 'None':
								data[sec]['insight'] = ins
							if sug !=  np.nan and sug != None and str(sug) != 'None':
								data[sec]['suggestion'] = sug				

		return data

	def parse_emotional_insight(self, data):
		data['insight'] = {}
		emotional_tone = data['tone_categories'][0]['tones']
		emotional_tone = sorted(emotional_tone, key=lambda k: k['score'], reverse = True) 
	
		if emotional_tone[0]['score'] > self.thresh:
			tone_name = emotional_tone[0]['tone_name']
		else:
			tone_name = 'None'
		
		data['insight']['EmotionalTone'] = {
		'message' : self.get_message2('EmotionalTone', tone_name),
		'name' : tone_name
		} 

		social_tone = data['tone_categories'][2]['tones']
		social_tone = sorted(social_tone, key=lambda k: k['score'], reverse = True) 
		
		tone_name = social_tone[0]['tone_name']
		
		data['insight']['SocialTone'] = {
			'message' : self.get_message2('SocialTone', tone_name)      ,
			'name' : tone_name 
		}   

		return data